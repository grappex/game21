using System;
using Nakama;
using Zenject;

public class GlobalInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        Container.DeclareSignal<PlaySignal>();
        Container.DeclareSignal<QuitSignal>();

        Container.Bind(typeof(IClient)).To<Client>().FromInstance(
            new Client("http", "2f38500a6dd0.ngrok.io", 80, "defaultkey", UnityWebRequestAdapter.Instance));

        Container.Bind(typeof(IGameManager), typeof(IInitializable)).To<GameManager>().AsSingle();

        Container.Bind(typeof(IAuthService)).To<AuthService>().AsSingle();
    }
}