﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class RulesController : MonoBehaviour
{
    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    public Button Back;


    void Start()
    {
        Back.onClick.AddListener(() => SceneManager.UnloadSceneAsync("Rules",
    UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));
    }
}
