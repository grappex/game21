﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class CreateController : MonoBehaviour
{
    // Deps
    [Inject]
    public IAuthService _authService;

    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    // Bind
    public Button BackButon;
    public Button SaveButton;
    public Button Tab1;
    public Button Tab2;
    public Button Tab3;
    public Button Tab4;
    public Button Tab5;
    public Button Tab6;
    public Button Tab7;
    Button[] Tabs;
    public Slider BetSlider;
    public Text BetText;
    public Slider MaxBetSlider;
    public Text MaxBetText;
    public Slider MoveTimeSlider;
    public Text MoveTimeText;

    // State
    int players = 1;
    float bet = 0.1F;
    float maxBet = 0.5F;
    float moveTime = 0.1F;
    bool busy;

    void Start()
    {
        Tabs = new Button[] { Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7 };

        for (int i = 0; i < Tabs.Length; i++)
        {
            var count = i + 1;
            Tabs[i].onClick.AddListener(() =>
            {
                players = count;
                Render();
            });
        }
        BackButon.onClick.AddListener(() => SceneManager.UnloadSceneAsync("Create",
                UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));

        SaveButton.onClick.AddListener(OnSave);

        BetSlider.onValueChanged.AddListener(value =>
        {
            bet = value;
            Render();
        });
        MaxBetSlider.onValueChanged.AddListener(value =>
        {
            maxBet = value;
            Render();
        });
        MoveTimeSlider.onValueChanged.AddListener(value =>
        {
            moveTime = value;
            Render();
        });

        BetSlider.value = bet;
        BetSlider.minValue = 0.1F;
        MaxBetSlider.value = maxBet;
        MaxBetSlider.minValue = 0.5F;
        MoveTimeSlider.value = moveTime;
        MoveTimeSlider.minValue = 0.1F;
        Render();
    }

    private void OnSave()
    {
        // Alert.text = "";
        busy = true;
        Render();
        try
        {
            // Call API
            SceneManager.UnloadSceneAsync("Create",
                UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
        catch (Exception)
        {
            //  Alert.text = "Что - то пошло не так! Пожалуйста, попытайтесь еще раз.";
        }
        busy = false;
        Render();
    }

    void Render()
    {
        for (int i = 0; i < Tabs.Length; i++)
        {
            var button = Tabs[i];
            var text = button.GetComponentInChildren<Text>();

            if (button.tag == $"{players}")
            {
                var colors = button.colors;
                colors.normalColor = new Color(255, 255, 255, (float)0.5);
                button.colors = colors;
                text.color = new Color(255, 255, 255);

            }
            else
            {
                var colors = button.colors;
                colors.normalColor = new Color(255, 255, 255, (float)0.15);
                button.colors = colors;
                text.color = new Color(255, 255, 255, (float)0.4);

            }
        }

        BetText.text = $"{(int)(bet * 1000)}";
        MaxBetText.text = $"{(int)(maxBet * 1000)}";
        MoveTimeText.text = $"{(int)(moveTime * 10)} минут";
    }
}
