﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class RegisterController : MonoBehaviour
{
    [Inject]
    public IAuthService _authService;

    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    //Bind
    public InputField EmailField;

    public InputField PasswordField;

    public InputField InviteField;

    public InputField UsernameField;

    public Text Alert;

    public Button Submit;

    public Button Back;

    public GameObject Progress;

    //State
    string password;

    string email;

    string invite;

    string username;

    bool busy;


    void Start()
    {
        EmailField.onValueChanged.AddListener(value => email = value);
        PasswordField.onValueChanged.AddListener(value => password = value);
        InviteField.onValueChanged.AddListener(value => invite = value);
        UsernameField.onValueChanged.AddListener(value => username = value);
        Back.onClick.AddListener(() => SceneManager.UnloadSceneAsync("Register",
            UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));
        Submit.onClick.AddListener(OnRegister);
    }

    async void OnRegister()
    {
        Alert.text = "";
        busy = true;
        try
        {
            await _authService.Register(email, password, invite, username);
            _sceneLoader.LoadScene("Home", LoadSceneMode.Single);
        }
        catch (Exception)
        {
            Alert.text = "Что - то пошло не так! Пожалуйста, попытайтесь еще раз.";
        }
        busy = false;
    }

    void Update()
    {
        Submit.interactable = !string.IsNullOrEmpty(password)
            && email.IsValidEmail()
            && !string.IsNullOrEmpty(invite)
            && !string.IsNullOrEmpty(username);
        Progress.SetActive(busy);
    }
}
