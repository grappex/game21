﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class TablesController : MonoBehaviour
{
    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    public Button Back;

    public Button Add;

    void Start()
    {
        Add.onClick.AddListener(() => _sceneLoader.LoadScene("Create", LoadSceneMode.Additive));
        Back.onClick.AddListener(() => SceneManager.UnloadSceneAsync("Tables",
    UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));
    }
}
