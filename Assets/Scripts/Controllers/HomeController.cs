﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class HomeController : MonoBehaviour
{
    //Deps
    [Inject]
    public ZenjectSceneLoader _sceneLoader;


    //Bind
    public Button Create;
    public Button Tales;
    public Button Rules;
    public Button Settings;

    void Start()
    {
        Create.onClick.AddListener(() => _sceneLoader.LoadSceneAsync("Create", LoadSceneMode.Additive));
        Tales.onClick.AddListener(() => _sceneLoader.LoadSceneAsync("Tables", LoadSceneMode.Additive));
        Rules.onClick.AddListener(() => _sceneLoader.LoadSceneAsync("Rules", LoadSceneMode.Additive));
        Settings.onClick.AddListener(() => _sceneLoader.LoadSceneAsync("Settings", LoadSceneMode.Additive));
    }

    void Update()
    {
        
    }
}
