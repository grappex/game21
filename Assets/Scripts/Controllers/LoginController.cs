﻿using Zenject;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class LoginController : MonoBehaviour
{
    [Inject]
    public IAuthService _authService;

    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    //Bind
    public InputField EmailField;

    public InputField PasswordField;

    public Text Alert;

    public Button Submit;

    public Button Register;

    public GameObject Progress;

    //State
    string password;

    string email;

    bool busy;


    void Start()
    {
        EmailField.onValueChanged.AddListener(value => email = value);
        PasswordField.onValueChanged.AddListener(value => password = value);
        Submit.onClick.AddListener(OnLogin);
        Register.onClick.AddListener(() => _sceneLoader.LoadScene("Register", LoadSceneMode.Additive));
    }

    async void OnLogin()
    {
        Alert.text = "";
        busy = true;
        try
        {
            await _authService.Authenticate(email, password);
            _sceneLoader.LoadScene("Home", LoadSceneMode.Single);
        }
        catch (Exception)
        {
            Alert.text = "Что - то пошло не так! Пожалуйста, попытайтесь еще раз.";
        }
        busy = false;
    }

    void Update()
    {
        Submit.interactable = !string.IsNullOrEmpty(password) && email.IsValidEmail();
        Progress.SetActive(busy);
    }

}
