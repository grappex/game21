﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class RootController : MonoBehaviour
{
    [Inject]
    public IAuthService _authService;

    [Inject]
    ZenjectSceneLoader _sceneLoader;

    void Start()
    {
        if (_authService.IsAuthenticated())
        {
            _sceneLoader.LoadScene("Home", LoadSceneMode.Single);
        }
        else
        {
            _sceneLoader.LoadScene("Login", LoadSceneMode.Single);
        }
    }
}
