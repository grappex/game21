﻿using System.Threading.Tasks;
using Nakama;
using UnityEngine;
using Zenject;

public interface IAuthService
{
    bool IsAuthenticated();

    Task Authenticate(string email, string password);

    Task Register(string email, string password, string invite, string username);

    void Logout();
}

public class AuthService : IAuthService
{
    private const string PREFR_KEY_SESSION = "nakama.session";

    [Inject]
    IClient _client;

    public bool IsAuthenticated()
    {
        var authToken = PlayerPrefs.GetString(PREFR_KEY_SESSION);
        return Session.Restore(authToken)?.IsExpired == false;      
    }

    public async Task Authenticate(string email, string password)
    { 
        var session = await _client.AuthenticateEmailAsync(email, password, null, false);
        PlayerPrefs.SetString(PREFR_KEY_SESSION, session.AuthToken);
        PlayerPrefs.Save();
    }

    public async Task Register(string email, string password, string invite, string username)
    {
        var session = await _client.AuthenticateEmailAsync(email, password, username);
        PlayerPrefs.SetString(PREFR_KEY_SESSION, session.AuthToken);
        PlayerPrefs.Save();
    }

    public void Logout()
    {
        PlayerPrefs.SetString(PREFR_KEY_SESSION, "");
        PlayerPrefs.Save();
    }
}
