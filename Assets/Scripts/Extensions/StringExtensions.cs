﻿using System;
using System.Net.Mail;

public static class StringExtensions
{
   public static bool IsValidEmail(this string email)
    {
        if (email?.IndexOf("@") <= 0) return false;

        try
        {
            var address = new MailAddress(email);
            return address.Address == email;
        }
        catch
        {
            return false;
        }
    }
}
