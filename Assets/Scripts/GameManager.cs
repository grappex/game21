﻿using System;
using Nakama;
using UnityEngine;
using Zenject;

public interface IGameManager
{
}

public class PlaySignal
{
}

public class QuitSignal
{
}

public class GameManager : IGameManager, IInitializable
{
    readonly SignalBus _signalBus;

    readonly ZenjectSceneLoader _sceneLoader;

    [Inject]
    IClient _client;

    public GameManager(SignalBus signalBus, ZenjectSceneLoader sceneLoader)
    {
        _signalBus = signalBus;
        _sceneLoader = sceneLoader;

        _signalBus.Subscribe<PlaySignal>(OnPlay);
        _signalBus.Subscribe<QuitSignal>(OnQuit);
    }

    private async void OnPlay(PlaySignal args)
    {

    }

    public void OnQuit(QuitSignal args)
    {
        Application.Quit();
    }

    public void Initialize()
    {
    }
}
