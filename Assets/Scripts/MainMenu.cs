﻿using System;
using UnityEngine;
using Zenject;

public class MainMenu : MonoBehaviour
{
    public event Action OnPlay;
    public event Action OnQuit;

    [Inject]
    public IGameManager _gameManager;

    [Inject]
    public SignalBus _signalBus;

    public void Play()
    {
        _signalBus.Fire(new PlaySignal());
    }

    public void Quit()
    {
        _signalBus.Fire(new QuitSignal());
    }
}