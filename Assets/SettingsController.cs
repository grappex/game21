﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class SettingsController : MonoBehaviour
{
    [Inject]
    public ZenjectSceneLoader _sceneLoader;

    [Inject]
    public IAuthService _authService;

    public Button Back;
    public Button Exit;

    void Start()
    {
        Back.onClick.AddListener(() => SceneManager.UnloadSceneAsync("Settings",
    UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));

        Exit.onClick.AddListener(OnExit);
    }

    private void OnExit()
    {

        _authService.Logout();
        _sceneLoader.LoadScene("Login", LoadSceneMode.Single);
    }
}
